import click
import lb_dev_project.data_handler.data_handler as dh

@click.group()
def lb_dev_project():
    pass

@lb_dev_project.command()
@click.option('-p', '--path', default='.')
def has_metadata(path):
    print(dh.metadata_exists(path))

@lb_dev_project.command()
@click.option('-p', '--path', default='.')
def read_metadata(path):
    try:
        print(f'saved metadata:\n{dh.get_metadata(path)}')
    except FileNotFoundError:
        print(f'path to repo ({path}) does not have a metadata file')