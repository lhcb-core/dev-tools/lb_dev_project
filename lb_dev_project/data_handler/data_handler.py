from lb_utils.git_utils import GitUtils
from lb_utils.file_utils import FileUtils
import yaml

METADATA_FILE_NAME = ".metadata"

def metadata_exists(path_to_repo='.'):
    repository_root = GitUtils.get_git_root(path_to_repo)

    filenames_in_root_path = FileUtils.get_filenames(repository_root)

    return METADATA_FILE_NAME.encode() in filenames_in_root_path

def create_metadata_file(path_to_repo='.'):
    repository_root = GitUtils.get_git_root(path_to_repo)
    metadata_file_path = f'{repository_root.decode()}/{METADATA_FILE_NAME}'

    FileUtils.create_file(metadata_file_path, '')

    return metadata_file_path 

def write_metadata(metadata, path_to_repo='.'):
    metadata_file_path = create_metadata_file(path_to_repo)

    with open(metadata_file_path, 'w') as file:
        yaml.dump(metadata, file)

def get_metadata(path_to_repo='.'):
    repository_root = GitUtils.get_git_root(path_to_repo)
    
    with open(f'{repository_root.decode()}/{METADATA_FILE_NAME}') as file:
        metadata = yaml.full_load(file)

        return metadata

def create_project_from_metadata(metadata=None, path_to_repo='.'):
    if metadata:
        metadata = metadata[0]
    else:
        metadata = get_metadata(path_to_repo)[0]